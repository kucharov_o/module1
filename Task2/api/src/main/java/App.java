final class App {

    private App() {
    }

    public final void main(String[] args) {
        boolean allPositiveNumbers = CustomUtils.isAllPositiveNumbers("1", "2");
        System.out.println(allPositiveNumbers);
    }
}
