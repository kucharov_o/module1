final class CustomUtils {

    private CustomUtils() {

    }

    public static boolean isAllPositiveNumbers(String... str) {
        if (str == null || str.length == 0)
            return false;

        for (String s : str) {
            if (!Utils.isPositiveNumber(s))
                return false;
        }
        return true;
    }
}
