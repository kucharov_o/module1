import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringUtilsTest {

    @Test
    void isPositiveNumber() {
        Assertions.assertFalse(StringUtils.isPositiveNumber("-1"));

        Assertions.assertFalse(StringUtils.isPositiveNumber("-578"));

        Assertions.assertTrue(StringUtils.isPositiveNumber("0"));

        Assertions.assertTrue(StringUtils.isPositiveNumber("578"));
    }
}
